# BowlingAPI #

Python Backend Tech Test

## Given Information ##

## User Stories ###
- As a developer I want an API to start a new game with either 1 or 2 players
- As a developer I want an API to input a user's results frame by frame
- As a developer I want to retrieve my current score and frame by frame history
- As a developer I want to specify a historic game and retrieve the results
- As a developer I want to see an end to end test that simulates a real game
- Deploy the application to a live environment for testing

### Stretch goals ###
- Ensure the server code is stateless


## Developement

Developed using the Flask web-framework, backed with Postgres (9.6). Ensure all the requirements found in requirements.txt file are installed to your virtual environment along with Postgres before running the application.

### Database

When running the application locally for the first time, a database will need to be created in Postgres called:

    bowling_api_dev

The database url should also be set in your environment like so:

    $env:DATABASE_URL="postgres://user:password@localhost:5432/bowlingapi_dev"

#### Local migrations

Any changes to files in the models directory, should be migrated via the following commands. These commands should also be completed on first set-up:

    python manage.py db init (only on first setup)

    python manage.py db migrate

    python manage.py db upgrade

### Run the application

Using Windows PowerShell:

    $env:FLASK_APP="bowling.app"

    $env:FLASK_ENV="development"

    $env:APP_SETTINGS="bowling.config.DevelopmentConfig"

    flask run

For other operating systems and command shells, please see the Flask documentation: http://flask.pocoo.org/docs/1.0/tutorial/factory/

You can then access the website in your browser via the link:

    http://127.0.0.1:5000/

### Testing

To test the application locally, run the following command in the root directory:

    python -m unittest discover

## Usage ##

The graphical user interface is still in-progress. However, you can perform the following functions with curl commands:

### New game ###

For a new game, the number of players (1 or 2) that are involved need to be specified in the url, e.g:

    curl -X POST http://127.0.0.1:5000/game/new/2

    curl -X POST https://lord-of-the-pins.herokuapp.com/game/new/2

### Inputting user scores frame-by-frame ###

To input the users scores-frame-by-frame, a game id must be provided, as well as some parameters.

   curl -X POST "https://lord-of-the-pins.herokuapp.com/game/update/21?player_number=$player_number$&frame_number=$frame_number$&first_throw=$first_throw$&second_throw=$second_throw$"

where you should change the variables enclosed with '$' appropriate values, more information on each parameter is below.

#### Parameters ####
- player_number = 1 or 2
- frame_number = 1 to 10
- first_throw = 0 to 10
- second_throw = 0 to 10 (optional. if first_throw = 10)
- fill_ball = 0 to 10 (optional. only allowed for frame 10)


An example curl request would look like the following:

    curl -X POST "https://lord-of-the-pins.herokuapp.com/game/update/21?player_number=1&frame_number=1&first_throw=4&second_throw=5"

### Checking a players current score in a game and frame-by-frame history

To check an individual players in-game score:

    curl -X GET https://lord-of-the-pins.herokuapp.com/game/in-play/$int:game_id$/player/$int:player_id$

For example:

    curl -X GET https://lord-of-the-pins.herokuapp.com/game/in-play/21/player/1

### View an historic game and its results ###

For full game history:

    curl -X GET https://lord-of-the-pins.herokuapp.com/game/history/$int:game_id$

For example:

    curl -X GET https://lord-of-the-pins.herokuapp.com/game/history/21
