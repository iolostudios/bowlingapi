import os

from flask import Flask
from .models.games import db
from .views import core

# create and configure the app
app = Flask(__name__)
app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# initialize db
db.init_app(app)

app.register_blueprint(core.bp)
app.add_url_rule('/', endpoint='index')
app.add_url_rule('/game/in-play/<int:game_id>', endpoint='current_game')
app.add_url_rule('/game/historic/<int:game_id>', endpoint='game_history')
app.add_url_rule('/game/new/<int:no_of_players>', endpoint='new_game')


if __name__ == '__main__':
    app.run()

# import models so they are picked up by manage.py migrate
from .models import Game, Frame