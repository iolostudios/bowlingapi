# #!flask/bin/python
import requests

from flask import url_for

from .test_base import BaseTestCase
from bowling.models import Game, Frame
from flask import url_for


class TestBowlingGameAPI(BaseTestCase):

    def test_full_game(self):

        # check no games beforehand
        game_before = Game.query.filter_by(number_of_players=1).count()
        self.assertEqual(game_before, 0)

        # start a game via the new game url
        response = self.client.post(url_for('core.newGame', no_of_players=1), content_type='application/json')
        self.assertEqual(response.status_code, 201)

        # check game created
        game_after = Game.query.filter_by(number_of_players=1)
        self.assertEqual(game_after.count(), 1)

        game = game_after.first()

        # add frame one scores
        response = self.client.post(url_for('core.updateFrame', game_id=game.id, player_number=1, frame_number=1,
                                    first_throw=8, second_throw=2),
                                    content_type='application/json')

        self.assertEqual(response.status_code, 201)

        frame_one = Frame.query.filter_by(game_id=game.id, player_no=1).first()
        self.assertEqual(frame_one.first_throw, 8)
        self.assertEqual(frame_one.second_throw, 2)
        self.assertEqual(frame_one.score, None)  # because a spare was hit

        # try repeating the frame
        response = self.client.post(url_for('core.updateFrame', game_id=game.id, player_number=1, frame_number=1,
                                    first_throw=8, second_throw=2),
                                    content_type='application/json')

        self.assertEqual(response.status_code, 403)

        # frame 2
        response = self.client.post(url_for('core.updateFrame', game_id=game.id, player_number=1, frame_number=2,
                                    first_throw=10),
                                    content_type='application/json')

        self.assertEqual(response.status_code, 201)
        data = response.data.decode("utf-8")

        assert "\"first_throw\":10" in data
        assert "\"score\":null" in data  # because a strike was hit

        # get current score for a player with a game id
        response = self.client.get(url_for('core.currentScore', game_id=game.id, player_id=1),
                                   content_type='application/json')
        data = response.data.decode("utf-8")

        assert "\"player_one_running_score\":20" in data
        assert "\"score\":20" in data  # first frame can be calculated now
        assert "\"score\":null" in data  # second frame a strike was hit


        # frame 3
        response = self.client.post(url_for('core.updateFrame', game_id=game.id, player_number=1, frame_number=3,
                                    first_throw=10),
                                    content_type='application/json')

        self.assertEqual(response.status_code, 201)
        data = response.data.decode("utf-8")

        assert "\"first_throw\":10" in data
        assert "\"score\":null" in data  # because a strike was hit

        # get current score for a player with a game id
        response = self.client.get(url_for('core.currentScore', game_id=game.id, player_id=1),
                                   content_type='application/json')
        data = response.data.decode("utf-8")

        assert "\"player_one_running_score\":20" in data
        assert "\"score\":20" in data  # first frame can be calculated now
        assert "\"score\":null" in data  # second frame a strike was hit

        # frame 4
        response = self.client.post(url_for('core.updateFrame', game_id=game.id, player_number=1, frame_number=4,
                                    first_throw=10),
                                    content_type='application/json')

        self.assertEqual(response.status_code, 201)
        data = response.data.decode("utf-8")

        assert "\"first_throw\":10" in data
        assert "\"score\":null" in data  # because a strike was hit

        # get current score for a player with a game id
        response = self.client.get(url_for('core.currentScore', game_id=game.id, player_id=1),
                                   content_type='application/json')
        data = response.data.decode("utf-8")

        assert "\"player_one_running_score\":50" in data
        assert "\"score\":20" in data  # first frame can be calculated now
        assert "\"score\":30" in data  # second frame a strike was hit - can be calculated now
        assert "\"score\":null" in data  # third/fourth frame a strike was hit

        # frame 5
        response = self.client.post(url_for('core.updateFrame', game_id=game.id, player_number=1, frame_number=5,
                                    first_throw=4, second_throw=2),
                                    content_type='application/json')

        self.assertEqual(response.status_code, 201)
        data = response.data.decode("utf-8")

        assert "\"first_throw\":4" in data
        assert "\"second_throw\":2" in data
        assert "\"score\":6" in data  # because open frame

        # get current score for a player with a game id
        response = self.client.get(url_for('core.currentScore', game_id=game.id, player_id=1),
                                   content_type='application/json')
        data = response.data.decode("utf-8")

        assert "\"player_one_running_score\":96" in data
        assert "\"score\":20" in data  # first frame can be calculated now
        assert "\"score\":30" in data  # second frame a strike was hit - can be calculated now
        assert "\"score\":24" in data  # third frame a strike was hit - can be calculated now
        assert "\"score\":16" in data  # fourth frame a strike was hit
        assert "\"score\":6" in data  # fifth frame an open frame

        # frame 6
        response = self.client.post(url_for('core.updateFrame', game_id=game.id, player_number=1, frame_number=6,
                                    first_throw=7, second_throw=3),
                                    content_type='application/json')

        self.assertEqual(response.status_code, 201)
        data = response.data.decode("utf-8")

        assert "\"first_throw\":7" in data
        assert "\"second_throw\":3" in data
        assert "\"score\":null" in data  # because a spare was hit

        # get current score for a player with a game id
        response = self.client.get(url_for('core.currentScore', game_id=game.id, player_id=1),
                                   content_type='application/json')
        data = response.data.decode("utf-8")

        assert "\"player_one_running_score\":96" in data
        assert "\"score\":20" in data  # first frame can be calculated now
        assert "\"score\":30" in data  # second frame a strike was hit - can be calculated now
        assert "\"score\":24" in data  # third frame a strike was hit - can be calculated now
        assert "\"score\":16" in data  # fourth frame a strike was hit
        assert "\"score\":6" in data  # fifth frame an open frame
        assert "\"score\":null" in data  # sixth frame an open frame

        # frame 7
        response = self.client.post(url_for('core.updateFrame', game_id=game.id, player_number=1, frame_number=7,
                                    first_throw=0, second_throw=0),
                                    content_type='application/json')
        self.assertEqual(response.status_code, 201)
        data = response.data.decode("utf-8")

        assert "\"first_throw\":0" in data
        assert "\"second_throw\":0" in data
        assert "\"score\":0" in data  # because a strike was hit

        # get current score for a player with a game id
        response = self.client.get(url_for('core.currentScore', game_id=game.id, player_id=1),
                                   content_type='application/json')
        data = response.data.decode("utf-8")

        assert "\"player_one_running_score\":106" in data
        assert "\"score\":20" in data  # first frame can be calculated now
        assert "\"score\":30" in data  # second frame a strike was hit - can be calculated now
        assert "\"score\":24" in data  # third frame a strike was hit - can be calculated now
        assert "\"score\":16" in data  # fourth frame a strike was hit
        assert "\"score\":6" in data  # fifth frame an open frame
        assert "\"score\":10" in data  # sixth frame an open frame
        assert "\"score\":0" in data  # seventh frame an open frame

        # frame 8
        response = self.client.post(url_for('core.updateFrame', game_id=game.id, player_number=1, frame_number=8,
                                    first_throw=0, second_throw=10),
                                    content_type='application/json')
        self.assertEqual(response.status_code, 201)
        data = response.data.decode("utf-8")

        assert "\"first_throw\":0" in data
        assert "\"second_throw\":10" in data
        assert "\"score\":null" in data  # because a strike was hit

        # get current score for a player with a game id
        response = self.client.get(url_for('core.currentScore', game_id=game.id, player_id=1),
                                   content_type='application/json')
        data = response.data.decode("utf-8")

        assert "\"player_one_running_score\":106" in data
        assert "\"score\":20" in data  # first frame can be calculated now
        assert "\"score\":30" in data  # second frame a strike was hit - can be calculated now
        assert "\"score\":24" in data  # third frame a strike was hit - can be calculated now
        assert "\"score\":16" in data  # fourth frame a strike was hit
        assert "\"score\":6" in data  # fifth frame an open frame
        assert "\"score\":10" in data  # sixth frame an open frame
        assert "\"score\":0" in data  # seventh frame an open frame
        assert "\"score\":null" in data  # eighth frame a spare was hit

        # frame 9
        response = self.client.post(url_for('core.updateFrame', game_id=game.id, player_number=1, frame_number=9,
                                    first_throw=8, second_throw=2),
                                    content_type='application/json')
        self.assertEqual(response.status_code, 201)
        data = response.data.decode("utf-8")

        assert "\"first_throw\":8" in data
        assert "\"second_throw\":2" in data
        assert "\"score\":null" in data  # because a strike was hit

        # get current score for a player with a game id
        response = self.client.get(url_for('core.currentScore', game_id=game.id, player_id=1),
                                   content_type='application/json')
        data = response.data.decode("utf-8")

        assert "\"player_one_running_score\":124" in data
        assert "\"score\":20" in data  # first frame can be calculated now
        assert "\"score\":30" in data  # second frame a strike was hit - can be calculated now
        assert "\"score\":24" in data  # third frame a strike was hit - can be calculated now
        assert "\"score\":16" in data  # fourth frame a strike was hit
        assert "\"score\":6" in data  # fifth frame an open frame
        assert "\"score\":10" in data  # sixth frame an open frame
        assert "\"score\":0" in data  # seventh frame an open frame
        assert "\"score\":18" in data  # eighth frame a spare was hit
        assert "\"score\":null" in data  # ninth frame a spare was hit

        # frame 9
        response = self.client.post(url_for('core.updateFrame', game_id=game.id, player_number=1, frame_number=10,
                                    first_throw=10, second_throw=9, fill_ball=7),
                                    content_type='application/json')
        self.assertEqual(response.status_code, 201)
        data = response.data.decode("utf-8")

        assert "\"first_throw\":10" in data
        assert "\"second_throw\":9" in data
        assert "\"fill_ball\":7" in data
        assert "\"score\":26" in data  # because a strike was hit

        # get current score for a player with a game id
        response = self.client.get(url_for('core.currentScore', game_id=game.id, player_id=1),
                                   content_type='application/json')
        data = response.data.decode("utf-8")

        assert "\"player_one_running_score\":170" in data
        assert "\"score\":20" in data  # first frame can be calculated now
        assert "\"score\":30" in data  # second frame a strike was hit - can be calculated now
        assert "\"score\":24" in data  # third frame a strike was hit - can be calculated now
        assert "\"score\":16" in data  # fourth frame a strike was hit
        assert "\"score\":6" in data  # fifth frame an open frame
        assert "\"score\":10" in data  # sixth frame an open frame
        assert "\"score\":0" in data  # seventh frame an open frame
        assert "\"score\":18" in data  # eighth frame a spare was hit
        assert "\"score\":20" in data  # ninth frame a spare was hit
        assert "\"score\":26" in data  # tenth frame

        return
