"""empty message

Revision ID: 5aac8270c432
Revises: 48999c11aba8
Create Date: 2018-09-01 16:40:27.735472

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5aac8270c432'
down_revision = '48999c11aba8'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('games', sa.Column('number_of_players', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('games', 'number_of_players')
    # ### end Alembic commands ###
