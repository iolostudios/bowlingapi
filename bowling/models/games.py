from datetime import datetime
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


# Bowling game model
class Game(db.Model):
    __tablename__ = "games"
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, default=datetime.utcnow())
    number_of_players = db.Column(db.Integer, default=1)
    player_one_running_score = db.Column(db.Integer, default=0)
    player_two_running_score = db.Column(db.Integer, default=0)

    def __init__(self, number_of_players):
        self.date = datetime.utcnow()
        self.number_of_players = number_of_players
        self.player_one_running_score = 0
        self.player_two_running_score = 0

    def to_dict(self):
        data = {
            'id': self.id,
            'date': self.date,
            'number_of_players': self.number_of_players,
            'player_one_running_score': self.player_one_running_score,
            'player_two_running_score': self.player_two_running_score
        }
        return data

    def __repr__(self):
        return '<Game %d>' % self.id


# Bowling frame model - one-to-many relationship with Game
class Frame(db.Model):
    __tablename__ = "frames"
    id = db.Column(db.Integer, primary_key=True)
    game_id = db.Column(db.Integer, db.ForeignKey(Game.id))  # game id - foreign key
    frame_no = db.Column(db.Integer, nullable=False)  # frame number
    player_no = db.Column(db.Integer, nullable=False)  # player number
    first_throw = db.Column(db.Integer, default=0)  # the number of pins hit on the first throw of the frame
    second_throw = db.Column(db.Integer, default=0)  # the number of pins hit on the second throw of the frame
    fill_ball =  db.Column(db.Integer, nullable=True)
    score = db.Column(db.Integer, nullable=True)  # frame score - can be null temporarily

    def __init__(self, game_id, frame_no, player_no, first_throw, second_throw, fill_ball, score):
        self.game_id = game_id
        self.frame_no = frame_no
        self.player_no = player_no
        self.first_throw = first_throw
        self.second_throw = second_throw
        self.fill_ball = fill_ball
        self.score = score

    def to_dict(self):
        data = {
            'id': self.id,
            'game_id': self.game_id,
            'frame_no': self.frame_no,
            'player_no': self.player_no,
            'first_throw': self.first_throw,
            'second_throw': self.second_throw,
            'fill_ball': self.fill_ball,
            'score': self.score
        }
        return data

    def __repr__(self):
        return '<Frame number %d>' % self.frame_no