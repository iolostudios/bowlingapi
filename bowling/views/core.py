from flask import Blueprint, render_template, jsonify, request

from bowling.models.games import Game, Frame
from bowling.app import db

bp = Blueprint('core', __name__)


@bp.route('/', methods=['GET'])
def index():
    games = Game.query.all()
    return render_template('index.html', games=games)


# given an integer value representing the desired number of players, create a new game
@bp.route('/game/new/<int:no_of_players>', methods=['POST'])
def newGame(no_of_players):

    if no_of_players < 1 or no_of_players > 2:
        response = jsonify({'forbidden': "Only allowed a 1 or 2 player game."})
        response.status_code = 403
        return response

    # create a new game object and set the number of players
    game = Game(number_of_players=no_of_players)
    # add game object to database
    db.session.add(game)
    db.session.commit()
    # return response
    response = jsonify(game.to_dict())
    response.status_code = 201
    return response


@bp.route('/game/in-play/<int:game_id>', methods=['GET'])
def currentGame(game_id):
    game = Game.query.get_or_404(game_id)
    if game:
        frames = Frame.query.filter_by(game_id=game_id).all()
        return render_template('current-game.html', game=game, frames=frames)
    else:
        return render_template('404.html')


@bp.route('/game/historic/<int:game_id>', methods=['GET'])
def historicGame(game_id):
    game = Game.query.get_or_404(game_id)
    if game:
        frames = Frame.query.filter_by(game_id=game_id)
        return render_template('historic-game.html', game=game, frames=frames)
    else:
        return render_template('404.html')


# scores current frame and any previous one
def scoreFrame(frame_no, first_throw, second_throw, third_throw=None):

    # if last frame, the result needs to be calculated
    if frame_no == 10:
        if third_throw or third_throw == 0:
            result = first_throw + second_throw + third_throw
        else:
            result = first_throw + second_throw
    elif not second_throw and second_throw != 0:
        result = None
    elif first_throw + second_throw != 10:
        result = first_throw + second_throw
    else:
        result = None

    return result


# update previous frames results if necessary
def updatePreviousFrames(game_id, frame_number, player_number, first_throw, second_throw):

    # if first frame, cannot check previous
    if frame_number == 1:
        return 0

    # if only second frame only go one back
    elif frame_number == 2:
        previous_frame = Frame.query.filter_by(game_id=game_id, frame_no=frame_number - 1,
                                               player_no=player_number).first()
        # check if previous frame already has a result
        if not previous_frame.score and previous_frame.score != 0:
            # if the previous frame was a strike, and the current frame was a strike
            if previous_frame.first_throw == 10 and first_throw == 10:
                return 0
            elif previous_frame.first_throw == 10:
                score = 10 + first_throw + second_throw
                previous_frame.score = score
            else:
                score = 10 + first_throw
                previous_frame.score = score
            return score

    # else get previous 2 frames
    else:
        frame_one = Frame.query.filter_by(game_id=game_id, frame_no=frame_number - 1,
                                          player_no=player_number).first()
        frame_two = Frame.query.filter_by(game_id=game_id, frame_no=frame_number - 2,
                                          player_no=player_number).first()
        f_one_score = 0
        f_two_score = 0
        # check if previous frame already has a result
        if not frame_one.score and frame_one.score != 0:
            # if the previous frame was a strike, then leave it.
            if frame_one.first_throw == 10 and first_throw == 10:
                f_one_score = 0
            elif frame_one.first_throw == 10:
                f_one_score = 10 + first_throw + second_throw
                frame_one.score = f_one_score
            else:
                f_one_score = 10 + first_throw
                frame_one.score = f_one_score
        # check if previous frame already has a result
        if not frame_two.score and frame_two.score != 0:
            # if the previous frame was a strike, then leave it.
            if frame_two.first_throw == 10 and frame_one.first_throw == 10:
                f_two_score = 10 + frame_one.first_throw + first_throw
                frame_two.score = f_two_score
            elif frame_two.first_throw == 10:
                f_two_score = 10 + frame_one.first_throw + frame_one.second_throw
                frame_two.score = f_two_score
            else:
                f_two_score = 10 + first_throw
                frame_two.score = f_two_score

        return f_one_score + f_two_score

    return 0


# check if parameters passed to update frame are sensible
def validateFrameParams(game_id, player_number, frame_number, first_throw, second_throw, fill_ball):

    game = Game.query.get_or_404(game_id)
    # check number of players
    if player_number < 1 or player_number > game.number_of_players:
        return False

    # there are only 10 frames
    if frame_number < 1 or frame_number > 10:
        return False
    # check if there is a previous frame
    if frame_number != 1:
        frame = Frame.query.filter_by(game_id=game_id, player_no=player_number, frame_no=frame_number-1).scalar()
        if not frame:
            return False

    # there are only 10 pins
    if first_throw < 0 or first_throw > 10:
        return False
    # if the first throw is not a strike, a second throw must be provided
    if first_throw != 10 and not second_throw and second_throw != 0:
        return False

    if second_throw:
        # if the first throw + second throw is greater than 10, then return false unless the frame number is 10
        if first_throw + second_throw > 10 and frame_number != 10:
            return False

    # frame 10 special case validation
    if frame_number == 10 and fill_ball == 0:
        if first_throw != 10 or first_throw + second_throw != 10:
            return False
        if fill_ball < 0 or fill_ball > 10:
            return False
    elif frame_number != 10 and fill_ball:
        return False

    return True


@bp.route('/game/update/<int:game_id>', methods=['POST'])
def updateFrame(game_id):

    # check if game exists first
    game = Game.query.get_or_404(game_id)

    # get required parameters
    player_number = request.args.get('player_number')
    frame_number = request.args.get('frame_number')
    first_throw = request.args.get('first_throw')
    second_throw = request.args.get('second_throw')
    fill_ball = request.args.get('fill_ball')  # special edge case -- only 10th frame

    # check if correct details are passed through with the request POST
    if not player_number:
        response = jsonify({'forbidden': "no player number provided"})
        response.status_code = 403
        return response
    if not frame_number:
        response = jsonify({'forbidden': "no frame number provided"})
        response.status_code = 403
        return response
    if not first_throw and first_throw != 0:
        response = jsonify({'forbidden': "no first throw provided"})
        response.status_code = 403
        return response
    # second throw and fill ball doesnt need to be provided. However if they are, convert to int
    if second_throw:
        second_throw = int(second_throw)
    if fill_ball:
        fill_ball = int(fill_ball)

    # check if frame parameters are sensible
    validated = validateFrameParams(game_id, int(player_number), int(frame_number), int(first_throw), second_throw,
                                    fill_ball)

    if not validated:
        response = jsonify({'forbidden': "parameters provided are not in scope with this API or are incorrect"})
        response.status_code = 403
        return response

    # check if frame number for the player doesnt already exists?
    frame = Frame.query.filter_by(game_id=game_id, player_no=int(player_number), frame_no=int(frame_number)).scalar()

    if frame:
        response = jsonify({'forbidden': "this frame has already been updated for the player"})
        response.status_code = 403
        return response

    result = scoreFrame(int(frame_number), int(first_throw), second_throw, fill_ball)

    # update previous frames if needs be and return the amount it has been updated by
    score = updatePreviousFrames(game_id, int(frame_number), int(player_number), int(first_throw), second_throw)

    new_frame = Frame(game_id, int(frame_number), int(player_number), int(first_throw), second_throw, fill_ball, result)
    # add new frame object to database
    db.session.add(new_frame)

    if result == None:
        frame_result = 0
    else:
        frame_result = result

    # update games running score
    if int(player_number) == 1:
        game.player_one_running_score = game.player_one_running_score + score + frame_result
    elif int(player_number) == 2:
        game.player_two_running_score = game.player_two_running_score + score + frame_result

    db.session.commit()

    # return response
    response = jsonify(new_frame.to_dict())
    response.status_code = 201
    return response


# given a game id and player id, return the game and the frame-by-frame history for that player
@bp.route('/game/in-play/<int:game_id>/player/<int:player_id>', methods=['GET'])
def currentScore(game_id, player_id):

    # check if game exists first
    game = Game.query.get_or_404(game_id)

    frames = Frame.query.filter_by(game_id=game_id, player_no=player_id).order_by('frame_no').all()
    frames_dict = [game.to_dict()]
    for f in frames:
        frames_dict.append(f.to_dict())

    # return response
    response = jsonify(frames_dict)
    response.status_code = 200
    return response


# given a game id, display game final scores and all of the frames
@bp.route('/game/history/<int:game_id>', methods=['GET'])
def gameHistory(game_id):

    # check if game exists first
    game = Game.query.get_or_404(game_id)

    frames = Frame.query.filter_by(game_id=game_id).order_by('frame_no').all()
    frames_dict = [game.to_dict()]
    for f in frames:
        frames_dict.append(f.to_dict())

    # return response
    response = jsonify(frames_dict)
    response.status_code = 200
    return response
